#include QMK_KEYBOARD_H

#ifdef PROTOCOL_LUFA
  #include "lufa.h"
  #include "split_util.h"
#endif
#ifdef SSD1306OLED
  #include "ssd1306.h"
#endif

#include "raw_hid.h"

#ifdef OLED_DRIVER_ENABLE

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
	if	(!is_keyboard_master())
		return	OLED_ROTATION_180;  // flips the display 180 degrees if offhand
	return	rotation;
}

bool	is_hid_connected	=	false;	// Flag indicating if we have a PC connection yet

uint8_t	screen_master_buffer[OLED_MATRIX_SIZE]	=	{0};	// Master screen buffer
bool	screen_master_buffer_ready	            =	false;	// Flag if the buffer is complete
int	    screen_master_buffer_index	            =	0;	    // Keep track of fragments

int	    options	=	2;	// How long we expect options to be

void raw_hid_receive(uint8_t *data, uint8_t length) {
	// PC connected, so set the flag to show a message on the master display
	is_hid_connected	=	true;
	if	(length > 1 && data[0] == 1) {
		// New connection so reset screen_master_buffer
		screen_master_buffer_index	=	0;

		// Send what the maximum bytes we can send/recieve is and how long options should be
		uint8_t send_data[RAW_EPSIZE]	=	{RAW_EPSIZE, options};
		raw_hid_send(send_data, sizeof(send_data));
		return;
	}
	// First byte if not 1 tells us how many fragments to expect
	if (length > 1  && data[0] > 1) {
		// Copy fragment into buffer
		memcpy((char*)&screen_master_buffer[screen_master_buffer_index * (length-options)], data+1, (length-options));
		screen_master_buffer_index++;
		// Reached max fragments
		if	(screen_master_buffer_index == data[0]-1) {
			// Reset the buffer index back
			screen_master_buffer_index	=	0;
			screen_master_buffer_ready	=	true;
		}
  	}
	return;
}

// Let's us know if we're connected
char	hid_info_str[24];
const char *write_hid(void) {
	snprintf(hid_info_str, sizeof(hid_info_str), "%s", is_hid_connected ? "connected." : " ");
	return hid_info_str;
}

void oled_task_user(void) {
	// We can write to the screen
	if	(screen_master_buffer_ready && is_keyboard_master()) {
		// Clear buffer
		oled_write("", false);
		// Write compiled buffer
		oled_write_raw((char*)screen_master_buffer, sizeof(screen_master_buffer));
		// Stop rendering
		screen_master_buffer_ready	=	false;
	}	else if (is_keyboard_master()) {
		// Let us know if we connected
		oled_write("", false);
		oled_write_ln(write_hid(), false);
	}	else {
		// Slave OLED
	}
}
#endif