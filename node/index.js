const hid = require('node-hid'),
	// Example animation
	cake = require('./cake')

const KEYBOARD_NAME = 'Lily58'
const KEYBOARD_USAGE_ID = 0x61
const KEYBOARD_USAGE_PAGE = 0xff60
const KEYBOARD_UPDATE_TIME = 1000

let RAW_EPSIZE = 32
let options = 2

let keyboard = null

function wait(ms) {
	return new Promise((resolve) => {
		setTimeout(resolve, ms)
	})
}

async function sendToKeyboard() {
	let bytes = RAW_EPSIZE - options
	// loop through each frame of the animation
	for (const [key, frame] of Object.entries(cake)) {
		// Split each frame into max allowed bytes
		// byte 1 unused, byte 2 tells us how many pieces we have
		for (let i = 0; i < frame.length; i += bytes) {
			// We add 1 to the length so it's not confused with a new connection
			length = Math.ceil(frame.length / bytes) + 1
			// Send the fragment so we reassemble the frame on the keyboard
			keyboard.write([0, length].concat(frame.slice(i, i + bytes)))
		}
		// A timeout could be needed, but I had no issues without
		// wait(1)
	}
}
function updateKeyboardScreen() {
	if (!keyboard) {
		// Search all devices for a matching keyboard
		for (const d of hid.devices()) {
			if (d.product === KEYBOARD_NAME && d.usage === KEYBOARD_USAGE_ID && d.usagePage === KEYBOARD_USAGE_PAGE) {
				// Create a new connection and store it as the keyboard
				keyboard = new hid.HID(d.path)
				console.log(`Keyboard connection established.`)

				keyboard.on('data', (e) => {
					// Set what the actual RAW_EPSIZE is
					RAW_EPSIZE = e[0]
				})

				// Tell keyboard we have a connection
				// 1st byte - unused
				// 2nd byte - 1 to indicate a new connection
				keyboard.write([0, 1])
				break
			}
		}
	} else {
		// We have a connection
		sendToKeyboard()
	}
}

//for looping purposes
setInterval(updateKeyboardScreen, KEYBOARD_UPDATE_TIME)
